/* eslint-disable camelcase */
import { describe, test, expect } from '@jest/globals';
import { apiProvider } from '../hw9Framework/index';

describe('Тестируем MailBoxLayer', () => {
  test('Пользователь может проверить Email на валидность', async () => {
    const response = await apiProvider().checkValid().checkEmail();
    const { format_valid } = response.body;
    expect(format_valid).toBe(true);
  });

  test.each([
    ['support@apilayer.com', true],
    ['supportapilayer.com', false],
    ['support@apilayercom', false],
    ['', undefined],
  ])('Проверяем ввод Email', async (Email, expected) => {
    const response = await apiProvider().checkParam(Email).checkEmailParam();
    const { format_valid } = response.body;
    expect(format_valid).toBe(expected);
  });

  test('Пользователь не может пользоваться сервисом без Access_Key', async () => {
    const response = await apiProvider().checkAccess().checkAccess();
    const { error } = response.body;
    expect(error.code).toBe(101);
  });
});
