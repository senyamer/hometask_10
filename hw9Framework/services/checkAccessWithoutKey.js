import supertest from 'supertest';
import { url } from '../config/index';
import { newEmail } from '../builder/newEmail';

const CheckAccessWithoutKey = function checkAccess() {
  this.checkAccess = async function getCheckAccess() {
    const response = await supertest(`${url}`)
      .get(`/api/check?access_key=AAAA&email=${newEmail}`)
      .set('Accept', 'application/json');
    return response;
  };
};

export { CheckAccessWithoutKey };
