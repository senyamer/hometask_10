import supertest from 'supertest';
import { accessKey, url } from '../config/index';

const CheckEmailParametric = function CheckEmailParametric(Email) {
  this.checkEmailParam = async function getCheckEmailParam() {
    const response = await supertest(`${url}`)
      .get(`/api/check?access_key=${accessKey}&email=${Email}`)
      .set('Accept', 'application/json');
    return response;
  };
};

export { CheckEmailParametric };
