import supertest from 'supertest';
import { accessKey, url } from '../config/index';
import { newEmail } from '../builder/newEmail';

const CheckEmailValid = function checkEmailValid() {
  this.checkEmail = async function getCheckEmail() {
    const response = await supertest(`${url}`)
      .get(`/api/check?access_key=${accessKey}&email=${newEmail}`)
      .set('Accept', 'application/json');
    return response;
  };
};

export { CheckEmailValid };
