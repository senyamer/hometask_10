import { CheckEmailValid, CheckEmailParametric, CheckAccessWithoutKey } from './services';

const apiProvider = () => ({
  checkValid: () => new CheckEmailValid(),
  checkParam: (Email) => new CheckEmailParametric(Email),
  checkAccess: () => new CheckAccessWithoutKey(),
});

export { apiProvider };
